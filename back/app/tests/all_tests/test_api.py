import json
import pytest

CART_ITEMS_URL = "http://localhost:8000/api/market/items/"
PRODUCTS_URL = "http://localhost:8000/api/market/product/"


@pytest.mark.api
@pytest.mark.django_db
def test_product_get(api_client_credentials):
    response_auth_user = api_client_credentials.get(PRODUCTS_URL)
    api_client_credentials.credentials()
    response_anonymous = api_client_credentials.get(PRODUCTS_URL)
    assert response_anonymous.status_code == response_auth_user.status_code == 200


@pytest.mark.api
@pytest.mark.django_db
def test_cart_items_get(api_client_credentials):
    response_auth_user = api_client_credentials.get(CART_ITEMS_URL)
    api_client_credentials.credentials()
    response_anonymous = api_client_credentials.get(CART_ITEMS_URL)
    assert response_anonymous.status_code == 401
    assert response_auth_user.status_code == 200


data_cart_items = [
    {"product": "1", "quantity": "1"},
    {"product": "2", "quantity": "-9"},
    {"product": "rfr", "quantity": "4"},
    {"product": "8", "quantity": "4"},
    {"product": "9", "quantity": "4"},
    {"product": "200", "quantity": "4000"},
    {"product": "0", "quantity": "4"},
    {"product": "-5", "quantity": "-4"},
]


@pytest.mark.api
@pytest.mark.parametrize("item", data_cart_items)
@pytest.mark.django_db
def test_cart_items_post(item, api_client_credentials):
    response_auth_user = api_client_credentials.post(
        CART_ITEMS_URL, json.dumps(item), content_type="application/json"
    )
    api_client_credentials.credentials()
    response_anonymous = api_client_credentials.post(
        CART_ITEMS_URL, json.dumps(item), content_type="application/json"
    )
    assert response_anonymous.status_code == 401
    if not item["product"].isdigit():
        assert response_auth_user.status_code == 400
    elif not item["quantity"].isdigit():
        assert response_auth_user.status_code == 400
    elif int(item["product"]) > 8 or int(item["product"]) <= 0:
        assert response_auth_user.status_code == 400
    elif int(item["quantity"]) <= 0:
        assert response_auth_user.status_code == 400
    else:
        assert response_auth_user.status_code == 201
