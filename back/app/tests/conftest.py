import os
import json
import pytest
from django.conf import settings


BASE_TEST_URL = "http://localhost:8000/api"
LOGIN_URL = BASE_TEST_URL + "/token/"


USERS = [{"email": "buyer@test.com", "password": "777"}]

# @pytest.fixture(scope="session")
# def django_db_setup():
#     settings.DATABASES["default"] = {
#         "USER": os.getenv("TEST_USER") or "",
#         "PASSWORD": os.getenv("TEST_USER_PASSWORD") or "",
#     }


@pytest.fixture
def api_client():
    from rest_framework.test import APIClient

    return APIClient()


@pytest.fixture(params=USERS)
def login_test_user(api_client, request):
    data_login = {
        "email": request.param["email"],
        "password": request.param["password"],
    }
    response = api_client.post(
        LOGIN_URL, json.dumps(data_login), content_type="application/json"
    )
    # yield
    token = json.loads(response.content).get("access")
    return token


# настроенный апи клиент
@pytest.fixture
def api_client_credentials(api_client, login_test_user):
    api_client.credentials(HTTP_AUTHORIZATION=f"Bearer {login_test_user}")
    return api_client
