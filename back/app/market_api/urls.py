from rest_framework.routers import DefaultRouter
from django.urls import path
from .views import ProductViewSet, CartViewSet, make_order

router = DefaultRouter()
router.register("product", ProductViewSet, basename="product")
router.register("items", CartViewSet, basename="items")
urlpatterns = [path("items/orders/", make_order)] + router.urls
