from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import transaction

from rest_framework import mixins, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import action

from .models import Product, CartItem, Cart, User
from .serializers import (
    ProductSerializer,
    CartItemSerializer,
    ReadCartItemSerializer,
    CartSerializer,
)


class ProductViewSet(ReadOnlyModelViewSet):

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (AllowAny,)


# Для зарегистрированных пользователей
class CartViewSet(mixins.ListModelMixin, mixins.DestroyModelMixin, GenericViewSet):

    queryset = CartItem.objects.select_related("product", "product__user").order_by("-id")
    permission_classes = (IsAuthenticated,)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(
            self.get_queryset().filter(
                cart__user__id=request.user.id, cart__status=Cart.Statuses.NEW.value
            )
        )
        products_data = self.get_serializer(queryset, many=True).data
        return Response(products_data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer_class()(data=request.data)
        if serializer.is_valid(raise_exception=True):
            args = {"user_id": request.user.id, "status": Cart.Statuses.NEW.value}
            cart = Cart.objects.filter(**args).first() or Cart.objects.create(**args)
            cart_item = CartItem.objects.filter(
                cart__user__id=request.user.id,
                cart__status=Cart.Statuses.NEW.value,
                product_id=request.data["product"],
            ).first()
            if not cart_item:
                cart_item = CartItem.objects.create(
                    cart_id=cart.id,
                    product_id=request.data["product"],
                    quantity=request.data["quantity"],
                )
            else:
                cart_item.quantity += int(request.data["quantity"])
                cart_item.save()
        return Response(
            self.get_serializer_class()(cart_item).data, status.HTTP_201_CREATED
        )

    def partial_update(self, request, pk=None):
        instance = self.get_object()
        if self.validate_instance(request.user.id, instance):
            serializer = self.get_serializer_class()(
                data=request.data, instance=self.get_object(), partial=True
            )
            serializer.is_valid(raise_exception=True)
            serializer.save()
            return Response(serializer.data)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def destroy(self, request, pk=None):
        instance = self.get_object()
        if self.validate_instance(request.user.id, instance):
            instance.delete()
            return Response("Item was delete.", status=status.HTTP_200_OK)
        return Response(status=status.HTTP_403_FORBIDDEN)

    def validate_instance(self, user_id, instance):
        return (
            instance.cart.user_id == user_id
            and instance.cart.status == Cart.Statuses.NEW.value
        )

    @action(
        detail=False,
        methods=["PUT"],
        url_path="clear",
        url_name="clear_all_cart_product_item",
    )
    def clear(self, request):
        cart = Cart.objects.filter(
            user_id=request.user.id, status=Cart.Statuses.NEW.value
        ).first()
        if cart:
            cart.cart_items.all().delete()
            return Response("Cart is clear.", status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    @action(
        detail=False,
        methods=["POST"],
        url_path="order",
        url_name="make_order",
    )
    def make_order(self, request):
        cart = Cart.objects.filter(
            user_id=request.user.id, status=Cart.Statuses.NEW.value
        ).first()
        if cart:
            cart.status = Cart.Statuses.PROCESSED.value
            cart.save()
            return Response("Оrder placed", status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        if self.action in ("create", "partial_update"):
            return CartItemSerializer
        return ReadCartItemSerializer


# Для незарегистрированных пользователей оформивших заказ
@api_view(["POST"])
def make_order(request):
    def validate():
        if not request.user.is_anonymous:
            return False
        serializer = CartItemSerializer(
            data=request.data["items"], many=True, partial=True
        )
        serializer.is_valid(raise_exception=True)
        try:
            validate_email(request.data["email"])
        except ValidationError:
            return False
        return serializer

    def create_user():
        password = User.objects.make_random_password()
        user = User.objects.create(
            email=request.data["email"], name=request.data["email"]
        )
        user.set_password(password)
        user.save()
        return (user, password)

    def create_order(user, sz):
        cart = Cart.objects.create(
            user_id=user.id, status=Cart.Statuses.PROCESSED.value
        )
        items = [
            CartItem(
                cart_id=cart.id, product=item["product"], quantity=item["quantity"]
            )
            for item in sz.validated_data
        ]
        CartItem.objects.bulk_create(items)

    sz = validate()
    if not sz:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    with transaction.atomic():
        user, password = create_user()
        create_order(user, sz)
    return Response(
        f"Ваш email {user.email}, Пароль {password}", status=status.HTTP_201_CREATED
    )
