from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, UserManager


class User(AbstractBaseUser, PermissionsMixin):
    class TypeUser(models.IntegerChoices):
        ADMIN = 1
        BUYER = 2
        SELLER = 3

    email = models.EmailField("Email", max_length=255, unique=True)
    name = models.CharField("Name user", max_length=255)
    user_type = models.IntegerField(
        choices=TypeUser.choices, default=TypeUser.BUYER.value
    )
    is_verified = models.BooleanField(default=False)
    verified_at = models.DateTimeField(default=None, null=True, db_index=True)
    is_active = models.BooleanField(default=True, db_index=True)

    USERNAME_FIELD = "email"
    objects = UserManager()

    @property
    def is_staff(self):
        return self.user_type == self.TypeUser.ADMIN.value

    def __str__(self) -> str:
        return f"User {self.name} - {self.email}"


class Product(models.Model):

    image_url = models.URLField("Url image product", max_length=255)
    title = models.CharField("Title product", max_length=255)
    description = models.TextField("Description product", null=True, blank=True)
    price = models.DecimalField("Price product", max_digits=10, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"Product {self.title} - {self.price}"


class Cart(models.Model):
    class Statuses(models.IntegerChoices):
        NEW = 1
        PROCESSED = 2
        PAID = 3
        COMPLETED = 4

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.IntegerField(
        "Status", choices=Statuses.choices, default=Statuses.NEW.value
    )

    class Meta:
        verbose_name = 'Заказ'
        verbose_name_plural = "Заказы"


class CartItem(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField("Quantity", default=1)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE, related_name="cart_items")
