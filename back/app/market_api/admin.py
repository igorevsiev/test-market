from django.contrib import admin
from .models import Product, User, Cart, CartItem


class CartItemInline(admin.TabularInline):
    model = CartItem


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "description",
        "price",
        "user",
    )

    search_fields = (
        "user__email",
        "title",
    )


@admin.register(User)
class ProductAdmin(admin.ModelAdmin):
    pass


@admin.register(Cart)
class CartAdmin(admin.ModelAdmin):
    inlines = [CartItemInline]
    list_display = (
        "id",
        "status",
        "user",
    )
    search_fields = (
        "user__email",
        "status",
    )
