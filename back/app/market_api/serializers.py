from rest_framework import serializers
from .models import User, Product, Cart, CartItem


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", "name"]


class ProductSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Product
        fields = "__all__"


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = "__all__"
        read_only_fields = ("cart",)


class ReadCartItemSerializer(CartItemSerializer):
    product = ProductSerializer()

    class Meta(CartItemSerializer.Meta):
        pass


class CartSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    cart_items = CartItemSerializer(many=True)

    class Meta:
        model = Cart
        fields = "__all__"
