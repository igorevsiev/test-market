import axios from 'axios'
import { Loading, Notify, LocalStorage } from 'quasar'
import urls from '../api/url'

let getAxios = function (method, url, params) {
    let args = [url]
    if (params) {
        args.push(params)
    }
    if (!params && ['post', 'put', 'putch'].includes(method)) {
        args.push({})
    }
    if (LocalStorage.has('token')) {
        args.push({
            headers: {
                'Authorization': `Bearer ${LocalStorage.getItem('token')}`,
            }
        })
    }
    return axios[method](...args)
}

let getterProducts = function () {
    return this.stateProduct
}

let getterCartItems = function () {
    return this.stateCartItems
}

let loadProductList = function () {
    let url = urls.GET_LIST_PRODUCT
    getAxios('get', url).then((response) => {
        if (response.status == 200) {
            this.stateProduct = response.data
        } else {
            Notify.create({
                color: 'red',
                message: response.statusText
            })
        }
        Loading.hide()
    })
}

let loadCartItems = function (islogin) {
    if (islogin) {
        let url = urls.ITEMS_CART
        getAxios('get', url).then((response) => {
            if (response.status == 200) {
                this.stateCartItems = response.data
            } else {
                Notify.create({
                    color: 'red',
                    message: response.statusText
                })
            }
        })
    } else {
        if (LocalStorage.has('testCart')) {
            this.stateCartItems = Object.values(LocalStorage.getItem('testCart'))
        }
    }
}

let actionCart = function (id, method, args) {
    const url = id ? urls.ITEMS_CART+id+'/' : urls.ITEMS_CART
    return getAxios(method, url, args).then((response) => {
        if ([200, 201].includes(response.status)) {
            if (method == 'delete') {
                this.stateCartItems = this.stateCartItems.filter(x=>x.id !== id)
            } else if (method == 'patch') {
                this.stateCartItems.map((obj) => {
                    if (obj.id === id) {
                        obj.quantity = args.quantity
                    }
                })
            } else if (['order', 'orders'].includes(id)) {
                this.stateCartItems = []
                if (id == 'orders') {
                    this.loginData = response.data
                }
            }
            Notify.create({
                color: 'green',
                message: response.statusText || response.data
            })
        } else {
            Notify.create({
                color: 'red',
                message: response.statusText
            })
        }
        Loading.hide()
    })
}

export default {
  stateProduct: [],
  stateCartItems: [],
  loadCartItems: loadCartItems,
  loadProductList: loadProductList,
  getterProducts: getterProducts,
  getterCartItems: getterCartItems,
  actionCart: actionCart,
  login: false,
  loginData: '',
}

