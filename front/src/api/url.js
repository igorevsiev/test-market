const BASE_URL = 'http://localhost:8000'

export default {
    GET_LIST_PRODUCT: `${BASE_URL}/api/market/product/`,
    ITEMS_CART: `${BASE_URL}/api/market/items/`,
    CLEAR_CART: `${BASE_URL}/api/market/items/clear/`,
    ADD_TO_CART: `${BASE_URL}/api/market/items/create/`,
    LOGIN_URL: `${BASE_URL}/api/token/`
}
