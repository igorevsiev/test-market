## Описание

Проект состоит из:
1) Бэкенд, выполнен на - Djanjo REST (папка back)
2) Фронтенд (для теста), выполнен на - Vue js (quasar) (папка back)
3) БД postgresql

Бэк и фронт добавил в один репозиторий (вообще в реальном проекте обычно разношу в разные и отдельно деплой)
Не добавлял (для упрощения) проксирующего сервера nginx, вообще можно и его добавить в докер композ)

Фронтенд - localhost:8080
Админка после установки - localhost:8000/admin
Логин и пароль суперпользователя для доступа
```
admin@test.com
777
```
Логин и пароль пользователя для теста (после установки):
```
buyer@test.com
777
```

## Установки и настройки

```bash
$ cd ./back
```

## Запуск всего проекта с корневой директории (с docker-compose.yml)
```bash
docker-compose up --build -d

#отдельный сервис
docker-compose up --build --force-recreate --no-deps -d api
#выключение
docker-compose down
#для создания с новой базой
docker-compose down -v
```

## Установка проекта для локальной разработки (бэкэнд)
```bash
$ sudo pacman -S python-poetry
$ yay -S python-poetry
$ paru -S python-poetry
```
or
```bash
$ pip install -U poetry
```

## Запуск приложения локально

```zsh
$ poetry install
# # активируем .env
# $ export $(grep -v '^#' .env | xargs)
$ poetry run python app/manage.py runserver
```

## Тестирование

```bash
$ poetry run pytest app
```
Запуск определенных тестов проекта например с маркером user:
```bash
$ poetry run pytest -v -l -m user
```